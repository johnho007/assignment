# assignment

1. Prerequisite
    - Install java 1.8
2. Start app
    - Go to folder contain file stockPriceService.jar and run command in terminal
        java -jar stockPriceService
3. Note
    For assignment #3:
    -  After run application, using path below to get data
    path: /api/v2/200dma?tickersymbol=ticket1,ticker2,ticker3
    example URL: localhost:8080/api/v2/200dma?tickersymbol=ticket1,ticker2,ticker3
    ticket1,ticker2,ticker3:  ticker symbol want to get data.
    - Or can use path below to see REST document
    path: /api-doc
    example URL: localhost:8080/api-doc
