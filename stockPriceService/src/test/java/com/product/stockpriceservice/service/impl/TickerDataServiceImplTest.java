package com.product.stockpriceservice.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.product.stockpriceservice.data.PriceData;
import com.product.stockpriceservice.data.ResponseMessage;
import com.product.stockpriceservice.data._200DMAData;
import com.product.stockpriceservice.service.TickerDataService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TickerDataServiceImplTest {

	@Resource(name = "tickerDataService")
	TickerDataService service;
	
	String dateFormatPattern = "yyyy-MM-dd";
	SimpleDateFormat dateFormat = new SimpleDateFormat(dateFormatPattern);
	
	@Before
	public void setUp() {
	}
	
	@Test
	public void getClosePrice_validateResult() throws ParseException
	{
		String tickerSymbol = "FB";
		String startDate = "2014-01-01";
		String endDate = "2014-02-01";
		String closePrice = service.getClosePrice(tickerSymbol, startDate, endDate);
		Gson gson = new Gson();
		PriceData result = gson.fromJson(closePrice, PriceData.class);
		
		Assert.assertEquals(tickerSymbol,result.getPrices().get(0).getTicker());
		
		Date startDateDate = dateFormat.parse(startDate);
		
		for(List<String> data :result.getPrices().get(0).getDateClose())
		{
			Date closeDate = dateFormat.parse(data.get(0));
			
			Assert.assertTrue(startDateDate.compareTo(closeDate) < 0);
		}
	}
	
	@Test
	public void get200dma_validateResult()
	{
		String tickerSymbol = "FB";
		String startDate = "2014-01-01";
		String closePrice = service.get200DMA(tickerSymbol, startDate);
		Gson gson = new Gson();
		_200DMAData result = gson.fromJson(closePrice, _200DMAData.class);
		
		Assert.assertEquals(tickerSymbol,result.get200dma().getTicker());
	}
	
	@Test
	public void get200dma_MultiTicker_validateResult()
	{
		String tickerSymbol = "FB";
		String startDate = "2014-01-01";
		String closePrice = service.get200DMA(Arrays.asList(tickerSymbol,"xyz"), startDate);
		Gson gson = new Gson();
		List<Object> result = gson.fromJson(closePrice, new TypeToken<ArrayList<Object>>(){}.getType());
		_200DMAData resultForFB = gson.fromJson(result.get(0).toString(), _200DMAData.class);
		
		ResponseMessage resultForXYZ = gson.fromJson(result.get(1).toString(), ResponseMessage.class);
		
		Assert.assertEquals(2,result.size());
		Assert.assertEquals(tickerSymbol,resultForFB.get200dma().getTicker());
		Assert.assertEquals(Integer.valueOf(404),resultForXYZ.getCode());
	}
}
