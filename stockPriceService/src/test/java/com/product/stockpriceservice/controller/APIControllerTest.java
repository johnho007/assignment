package com.product.stockpriceservice.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class APIControllerTest {
	@Autowired
	private MockMvc mvc;

	@Test
	public void getPrice_ok() throws Exception {
		this.mvc.perform(get("/api/v2/FB/closePrice?startDate=2014-01-01&endDate=2014-02-01")).andExpect(status().isOk());
	}
	
	@Test
	public void getPrice_404() throws Exception {
		this.mvc.perform(get("/api/v2/FB/closePrice?startDate=2014-02-01&endDate=2014-01-01")).andExpect(status().is(404));
		
		this.mvc.perform(get("/api/v2/1234/closePrice?startDate=2014-01-01&endDate=2014-02-01")).andExpect(status().is(404));
	}
	
	@Test
	public void get200dma_ok() throws Exception {
		this.mvc.perform(get("/api/v2/FB/200dma?startDate=2014-01-01")).andExpect(status().isOk());
	}
	
	@Test
	public void get200dma_404() throws Exception {
		this.mvc.perform(get("/api/v2/FB/200dma?startDate=9999-99-99")).andExpect(status().is(404));
	}
	
	@Test
	public void get200DMAMultiTicker_ok() throws Exception {
		this.mvc.perform(get("/api/v2/200dma?startDate=2014-01-01&tickersymbol=FB,1234")).andExpect(status().isOk());
	}
	
	@Test
	public void get200DMAMultiTicker_WrongStartDate() throws Exception {
		this.mvc.perform(get("/api/v2/FB/200dma?startDate=9999-99-99")).andExpect(status().is(404));
	}
}
