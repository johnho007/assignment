package com.product.stockpriceservice.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseMessage {

	@SerializedName("code")
	@Expose
	private Integer code;
	@SerializedName("message")
	@Expose
	private String message;
	
	public ResponseMessage() {
		super();
	}

	public ResponseMessage(Integer code, String message) {
		super();
		this.code = code;
		this.message = message;
	}

	private final static long serialVersionUID = -3975246566712540754L;

	public Integer getCode() {
	return code;
	}

	public void setCode(Integer code) {
	this.code = code;
	}

	public String getMessage() {
	return message;
	}

	public void setMessage(String message) {
	this.message = message;
	}

}
