
package com.product.stockpriceservice.data;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Price implements Serializable
{

    @SerializedName("Ticker")
    @Expose
    private String ticker;
    @SerializedName("DateClose")
    @Expose
    private List<List<String>> dateClose = null;
    private final static long serialVersionUID = -2305316019103790183L;

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public List<List<String>> getDateClose() {
        return dateClose;
    }

    public void setDateClose(List<List<String>> dateClose) {
        this.dateClose = dateClose;
    }

}
