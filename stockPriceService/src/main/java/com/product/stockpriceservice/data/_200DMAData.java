
package com.product.stockpriceservice.data;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class _200DMAData implements Serializable
{

    @SerializedName("200dma")
    @Expose
    private _200DMA _200dma;
    private final static long serialVersionUID = 4194201927117433485L;

    public _200DMA get200dma() {
        return _200dma;
    }

    public void set200dma(_200DMA _200dma) {
        this._200dma = _200dma;
    }

}
