
package com.product.stockpriceservice.data;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TimeSeriesData implements Serializable
{

    @SerializedName("dataset")
    @Expose
    private Dataset dataset;
    private final static long serialVersionUID = -477449290286944181L;

    public Dataset getDataset() {
        return dataset;
    }

    public void setDataset(Dataset dataset) {
        this.dataset = dataset;
    }

}
