
package com.product.stockpriceservice.data;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class _200DMA implements Serializable
{

    @SerializedName("Ticker")
    @Expose
    private String ticker;
    @SerializedName("Avg")
    @Expose
    private String avg;
    private final static long serialVersionUID = -7210911565391839591L;

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public String getAvg() {
        return avg;
    }

    public void setAvg(String avg) {
        this.avg = avg;
    }

}
