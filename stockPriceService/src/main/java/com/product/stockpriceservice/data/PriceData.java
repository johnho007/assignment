
package com.product.stockpriceservice.data;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PriceData implements Serializable
{

    @SerializedName("Prices")
    @Expose
    private List<Price> prices = null;
    private final static long serialVersionUID = -4116975837768721040L;

    public List<Price> getPrices() {
        return prices;
    }

    public void setPrices(List<Price> prices) {
        this.prices = prices;
    }

}
