package com.product.stockpriceservice.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.product.stockpriceservice.service.TickerDataService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/api/v2")
@Api(description = "Set of endpoints for Retrieving Ticket Price Data /api/v2")
public class APIController {

	@Autowired
	TickerDataService tickerDataService;

	@ApiOperation("get ticker close price | path : /api/v2/{tickersymbol}/closePrice?startDate=YYYY-MM-DD&endDate=YYYY-MM-DD")
	@RequestMapping(value = "/{tickersymbol:.*}/closePrice", produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE }, method = RequestMethod.GET)
	public String getPrice(@PathVariable(value = "tickersymbol") String tickerSymbol,
			@RequestParam(value = "startDate", required = false) String startDate,
			@RequestParam(value = "endDate", required = false) String endDate) {

		return tickerDataService.getClosePrice(tickerSymbol, startDate, endDate);
	}

	@ApiOperation("get ticker200dma | path : /api/v2/{tickersymbol}/200dma?startDate=YYYY-MM-DD")
	@RequestMapping(value = "/{tickersymbol:.*}/200dma", produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE }, method = RequestMethod.GET)
	public String get200DMA(@PathVariable(value = "tickersymbol") String tickerSymbol,
			@RequestParam(value = "startDate", required = false) String startDate) {

		return tickerDataService.get200DMA(tickerSymbol, startDate);
	}

	@ApiOperation("get MULTI tickers 200dma | path : /api/v2/200dma?tickersymbol=ticket1,ticker2,ticker3")
	@RequestMapping(value = "/200dma", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE }, method = RequestMethod.GET)
	public String get200DMAMultiTicker(@RequestParam(value = "tickersymbol") String tickerSymbol,
			@RequestParam(value = "startDate", required = false) String startDate) {
		List<String> tickerSymbols = Arrays.asList(tickerSymbol.split(","));
		return tickerDataService.get200DMA(tickerSymbols, startDate);
	}
}
