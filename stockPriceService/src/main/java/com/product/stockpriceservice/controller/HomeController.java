package com.product.stockpriceservice.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import springfox.documentation.annotations.ApiIgnore;

@Controller
public class HomeController {
	
	@ApiIgnore
	@RequestMapping(value = "/api-doc", method = RequestMethod.GET)
	public String getDocument() {
		return "redirect:/swagger-ui.html";
	}
}
