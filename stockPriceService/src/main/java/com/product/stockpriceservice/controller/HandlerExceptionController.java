package com.product.stockpriceservice.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.product.stockpriceservice.data.ResponseMessage;

@RestControllerAdvice
public class HandlerExceptionController {

	@ExceptionHandler(Exception.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	protected ResponseMessage handleEntityNotFound(Exception ex) {
		ResponseMessage response = new ResponseMessage(404, ex.getMessage());
		response.setMessage(ex.getMessage());
		return response;
	}
}
