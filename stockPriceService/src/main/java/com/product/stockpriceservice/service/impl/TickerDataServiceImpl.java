package com.product.stockpriceservice.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.client.AsyncRestTemplate;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.google.gson.Gson;
import com.product.stockpriceservice.converter.Converter;
import com.product.stockpriceservice.data.PriceData;
import com.product.stockpriceservice.data.ResponseMessage;
import com.product.stockpriceservice.data.TimeSeriesData;
import com.product.stockpriceservice.data._200DMAData;
import com.product.stockpriceservice.service.TickerDataService;

@Service("tickerDataService")
public class TickerDataServiceImpl implements TickerDataService {

	@Value("${timeseriesdata.close_column_index}")
	String closeColumn;

	@Value("${timeseriesdata.database.code}")
	String databaseCode;

	@Value("${timeseriesdata.url.template}")
	String urlTemplate;

	@Value("${quandl.apikey}")
	String apiKey;

	@Resource()
	Converter<TimeSeriesData, PriceData> timeSeriesDataToPriceDataConverter;

	@Resource()
	Converter<TimeSeriesData, _200DMAData> timeSeriesDataCumulTo200DMADataConverter;

	@Override
	@Cacheable("CloseTicker")
	public String getClosePrice(String ticketSymbol, String startDate, String endDate) {

		RestTemplate restTemplate = new RestTemplate();

		String url = UriComponentsBuilder.fromUriString(String.format(urlTemplate, databaseCode, ticketSymbol))
				.queryParam("start_date", startDate).queryParam("end_date", endDate)
				.queryParam("column_index", closeColumn).toUriString();

		ResponseEntity<String> data = restTemplate.getForEntity(url, String.class);
		Gson gson = new Gson();
		TimeSeriesData timeSeriesData = gson.fromJson(data.getBody(), TimeSeriesData.class);

		PriceData priceData = timeSeriesDataToPriceDataConverter.convert(timeSeriesData);

		return gson.toJson(priceData);
	}

	@Override
	public String get200DMA(String ticketSymbol, String startDate) {

		RestTemplate restTemplate = new RestTemplate();

		String url = UriComponentsBuilder.fromUriString(String.format(urlTemplate, databaseCode, ticketSymbol))
				.queryParam("start_date", startDate).queryParam("column_index", closeColumn).queryParam("limit", 200)
				.queryParam("order", "asc").toUriString();

		ResponseEntity<String> data = restTemplate.getForEntity(url, String.class);
		Gson gson = new Gson();

		TimeSeriesData timeSeriesData = gson.fromJson(data.getBody(), TimeSeriesData.class);

		_200DMAData dmaData = timeSeriesDataCumulTo200DMADataConverter.convert(timeSeriesData);

		return gson.toJson(dmaData);
	}

	@Override
	public String get200DMA(List<String> ticketSymbols, String startDate) {
		List<ListenableFuture<ResponseEntity<String>>> futureResponses = new ArrayList<>();
		List<Object> _200dmaDatas = new ArrayList<>();
		AsyncRestTemplate restTemplate = new AsyncRestTemplate();

		for (String ticketSymbol : ticketSymbols) {
			String url = UriComponentsBuilder.fromUriString(String.format(urlTemplate, databaseCode, ticketSymbol))
					.queryParam("start_date", startDate).queryParam("column_index", closeColumn)
					.queryParam("limit", 200).queryParam("order", "asc").toUriString();

			System.out.println("fetch data" + ticketSymbol);

			futureResponses.add(restTemplate.getForEntity(url, String.class));
		}

		Gson gson = new Gson();
		for (int i = 0; futureResponses.size() > i; i++) {
			ListenableFuture<ResponseEntity<String>> futureResponse = futureResponses.get(i);
			ResponseEntity<String> data;
			try {
				data = futureResponse.get();

				TimeSeriesData timeSeriesData = gson.fromJson(data.getBody(), TimeSeriesData.class);
				_200dmaDatas.add(timeSeriesDataCumulTo200DMADataConverter.convert(timeSeriesData));
			} catch (InterruptedException | ExecutionException e) {
				_200dmaDatas.add(new ResponseMessage(404, "TICKER_NOT_FOUND_" + ticketSymbols.get(i)));
			}

		}
		return gson.toJson(_200dmaDatas);
	}
}
