package com.product.stockpriceservice.service;

import java.util.List;

public interface TickerDataService {

	String get200DMA(String tickerSymbol, String startDate);

	String getClosePrice(String tickerSymbol, String startDate, String endDate);

	String get200DMA(List<String> tickerSymbols, String startDate);
}
