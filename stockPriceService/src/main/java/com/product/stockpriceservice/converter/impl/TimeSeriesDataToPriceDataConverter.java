package com.product.stockpriceservice.converter.impl;

import java.util.Collections;

import org.springframework.stereotype.Service;

import com.product.stockpriceservice.converter.Converter;
import com.product.stockpriceservice.data.Dataset;
import com.product.stockpriceservice.data.Price;
import com.product.stockpriceservice.data.PriceData;
import com.product.stockpriceservice.data.TimeSeriesData;

@Service(value="timeSeriesDataToPriceDataConverter")
public class TimeSeriesDataToPriceDataConverter implements Converter<TimeSeriesData, PriceData> {

	@Override
	public PriceData convert(TimeSeriesData source) {
		PriceData data = new PriceData();
		Price price = new Price();

		data.setPrices(Collections.singletonList(price));
		Dataset dataset = source.getDataset();
		price.setTicker(dataset.getDatasetCode());
		price.setDateClose(dataset.getData());

		return data;
	}

}
