package com.product.stockpriceservice.converter;

public interface Converter<S,T> {

	T convert(S source);
}
