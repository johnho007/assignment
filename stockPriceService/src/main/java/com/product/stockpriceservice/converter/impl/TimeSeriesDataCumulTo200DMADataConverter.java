package com.product.stockpriceservice.converter.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.product.stockpriceservice.converter.Converter;
import com.product.stockpriceservice.data.Dataset;
import com.product.stockpriceservice.data.TimeSeriesData;
import com.product.stockpriceservice.data._200DMA;
import com.product.stockpriceservice.data._200DMAData;

@Service(value="timeSeriesDataCumulTo200DMADataConverter")
public class TimeSeriesDataCumulTo200DMADataConverter implements Converter<TimeSeriesData, _200DMAData> {

	@Override
	public _200DMAData convert(TimeSeriesData source) {
		_200DMAData dmadata = new _200DMAData();
		_200DMA _200dma = new _200DMA();

		dmadata.set200dma(_200dma);
		Dataset dataset = source.getDataset();
		
		List<List<String>> data = dataset.getData();
		
		
		Double average = data.stream().map(i -> Double.parseDouble(i.get(1))).mapToDouble(Double::doubleValue).average().getAsDouble();
		
		_200dma.setTicker(dataset.getDatasetCode());
		_200dma.setAvg(average.toString());

		return dmadata;
	}

}
